<?php
if(!isset($_SESSION)){
    session_start();
}
defined("CLASSES_DIR") || define("CLASSES_DIR", "classes");
defined("INC_DIR") || define("INC_DIR", "includes");
defined("ADMIN_DIR") || define("ADMIN_DIR", "admin");
defined("DS") || define("DS", DIRECTORY_SEPARATOR);
defined("ROOT_PATH") || define("ROOT_PATH", realpath(dirname(__FILE__)));

set_include_path(implode(PATH_SEPARATOR, array(
    realpath(ROOT_PATH.DS.CLASSES_DIR),
    realpath(ROOT_PATH.DS.INC_DIR),
    get_include_path()
)));
//nvm
?>

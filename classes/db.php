<?php
class db {
    private $host= 'localhost';
    private $user = 'root';
    private $password = 'viesturs123';
    private $database = 'gorbans';
    public $connection;
    public $affectedRows = 0;
    public $lastQuery;
    
    function __construct() {
        $this->connect();
    }
    function connect(){
        if($this->connection = mysqli_connect($this->host, $this->user, $this->password, $this->database)){
        }else{
            echo mysqli_error($connection);
            die();
        }
    }
    
    function sanitize($string){
        $string = mysqli_real_escape_string($this->connection, $string);
        $string = strip_tags($string);
        return $string;
    }
    
    function query($query){
        $this->lastQuery = $query;
        $result = mysqli_query($this->connection, $query);
        $this->affectedRows = mysqli_affected_rows($this->connection);
        return $result;
    }
    
    function fetchAll($query){
        $result = $this->query($query);
        return $result;
    }
}

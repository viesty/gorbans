<?php
require_once '../autoload.php';
$db = new db();
$skip = $_POST['skip'];
$skip = $db->sanitize($skip);
$skip = filter_var($_POST['skip'], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
if(!is_numeric($skip)){
    header('HTTP/1.1 500 Invalid skip count!');
    echo $skip;
    exit();
}
$skip*=6;
$perPage = 6;
$query = "SELECT * FROM `gorbans` ORDER BY `id` DESC LIMIT $skip, $perPage";
$result = $db->fetchAll($query);
$numRows = $db->affectedRows;
while($row = $result->fetch_assoc()){
    if($row['accepted'] === "1"){
        if($numRows%2===0){
        ?>
        <div class="row">
            <div class="columns small-12 medium-12 large-12">
                <img src="gorbis.png" class="round">
                <div class="bubble">
                    <p><?php echo $row['content'] ?></p>
                </div>
            </div>
        </div>
        <?php
        }else{
        ?>
        <div class="row">
            <div class="columns small-12 medium-12 large-12">
                <img src="gorbis.png" class="round right">
                <div class="rightbubble">
                    <p><?php echo $row['content'] ?></p>
                </div>
            </div>
        </div>
        <?php
        }
    }
    $numRows--;
}
?>
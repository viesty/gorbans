<?php require_once 'analytics.php'; ?>

<nav class="top-bar" data-topbar role="navigation">
  <ul class="title-area">
    <li class="name">
        <h1><a href="index.php">Gorbāna izteicieni</a></h1>
    </li>
    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
  </ul>
  <section class="top-bar-section">
    <ul class="left">
        <li><a href="#" class="button radius success" id="expandBtn">Pievienot izteicienu</a></li>
    </ul>
  </section>
</nav>

<div class="row" id="addComment">
    <div class="columns small-12 medium-12 large-12">
        <div class="columns small-12 medium-12 large-12">
            <label><span style="color:#FFF">Ieraksti kādu Gorbāna izteicienu:</span> 
            <input type="text" id="commentField">
        </label>
        </div>
        <div class="columns small-12 medium-12 large-12">
            <a href="#" class="button expand" id="addCommentBtn">Pievienot</a>
        </div>
        <div class="columns small-12 medium-12 large-12" id="errorAlert">
            <div data-alert class="alert-box alert">
                Kļūda ievadē. Pārbaudi tekstu!
            </div>
        </div>
        <div class="columns small-12 medium-12 large-12" id="successAlert">
            <div data-alert class="alert-box success">
                Izteiciens iesūtīts, tas tuvākajā laikā tiks (varbūt) apstiprināts un pievienots! :)
            </div>
        </div>
    </div>
</div>
<br>
<div class="row" id="commentDiv">
<?php //require_once 'populateMessages.php'; ?>
</div>
<div class="row" id="loading">
    <div class="columns small-12 medium-12 large-12 text-center">
        <img src="ajax-loader.gif" />
    </div>
</div>
<div class="row" id="pageend">
    <div class="columns small-12 medium-12 large-12 text-center">
        <p style="color:#FFF">Tu esi sasniedzis beigas, tālāk nekā nav :(</p>
    </div>
</div>
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/jquery.mobile-1.4.4.js"></script>
    <script>
        $(document).foundation();
    </script>
    
    <script>
        $(document).ready(function(){
            $('#addComment').hide();
            $('#errorAlert').hide();
            $('#successAlert').hide();
            $('#expandBtn').click(function(){
               $('#addComment').slideToggle('fast'); 
               $('#errorAlert').hide();
               $('#successAlert').hide();
            });
            $('#addCommentBtn').click(function(){
                var comment = $.trim($('#commentField').val());
                if(comment.length < 1){
                    $('#errorAlert').slideDown('fast');
                    return false;
                }
                var dataString = 'comment='+comment;
                $.ajax({
                  type: "POST",
                  url: "process.php",
                  data: dataString,
                  success: function() {
                      $('#successAlert').slideDown('fast');
                  }
                });
                $('#errorAlert').hide();
                $('#commentField').val("");
                return false;
            });
        });
        
        $(document).ready(function(){
            $('#loading').hide();
            $('#pageend').hide();
            var isDataAvailable = true;
            var counter = 0;
            $('#commentDiv').load('includes/populateMessages.php', {skip:counter});
            counter++;

            $(document).on("scrollstop", function (e) {

                /* active page */
                var activePage = $.mobile.pageContainer.pagecontainer("getActivePage"),
                
                /* window's scrollTop() */
                scrolled = $(window).scrollTop(),

                /* viewport */
                screenHeight = $.mobile.getScreenHeight(),

                /* content div height within active page */
                contentHeight = $("#commentDiv", activePage).outerHeight(),
                /* total height to scroll */
                scrollEnd = contentHeight - screenHeight;
            /* if total scrolled value is equal or greater
               than total height of content div (total scroll)
               and active page is the target page (pageX not any other page)
               call function */
            if (activePage[0].id == "" && scrolled >= scrollEnd) {
                if(isDataAvailable){
                        $('#loading').show();
                         $.post('includes/populateMessages.php', {skip:counter}, function(data){
                            $('#commentDiv').append(data);
                            if(data==''){ 
                                isDataAvailable = false; 
                                $('#pageend').show();
                            }
                        });
                        counter++;
                        $('#loading').hide();
                    }else{
                        $('#pageend').show();
                    }
            }
        });
            
        });
    </script>
    </body>
</html>
